# SoundBox Desktop V2

This is a refresh of a previous private version.  
SoundBox use the popular [F#](https://fsharp.org/) compiler [Fable](https://fable.io),  
[Sass](https://sass-lang.com/), a [CSS](https://developer.mozilla.org/fr/docs/Web/CSS) preprocessor  
and [ElectronJS](https://electronjs.org), a super web page.

## Installation
### Linux

Use [Snapcraft](https://snapcraft.io/) for download SoundBox.

```bash
sudo snap install soundbox --beta
```

### Windows
Install from [GitLab release](https://gitlab.com/gael.bouquain/soundbox-desktop-v2/-/releases) (It's a portable app).