module Sounds

open Fable.Core
open Fable.Core.JsInterop

type ButtonPosition =
    | Left
    | Middle
    | Right


[<Emit("new Audio($0)")>]
let newAudio (_path: string) : Browser.Types.HTMLAudioElement = jsNative


type SoundObject =
    { path: string
      name: string
      mutable isPlaying: bool
      imagePath: string
      position: ButtonPosition
      audioElement: Browser.Types.HTMLAudioElement }

type SpecialSoundObject =
  { path: string
    name: string
    mutable isPlaying: bool
    position: ButtonPosition
    audioElement: Browser.Types.HTMLAudioElement }



let dejaVu =
    { path = "../assets/Sounds/deja_vu.mp3"
      name = "dejaVu"
      isPlaying = false
      position = Middle
      audioElement = newAudio "../assets/Sounds/deja_vu.mp3" }

let boom =
    { path = "../assets/Sounds/boom.mp3"
      name = "boom"
      isPlaying = false
      position = Middle
      audioElement = newAudio "../assets/Sounds/boom.mp3" }


let sncf =
    { path = "../assets/Sounds/sncf.mp3"
      name = "sncf"
      isPlaying = false
      imagePath = "../assets/Images/logo-sncf.png"
      position = Left
      audioElement = newAudio "../assets/Sounds/sncf.mp3" }

let ah =
    { path = "../assets/Sounds/ah.mp3"
      name = "ah"
      isPlaying = false
      imagePath = "../assets/Images/ah.png"
      position = Middle
      audioElement = newAudio "../assets/Sounds/ah.mp3" }

let toca =
    { path = "../assets/Sounds/tocata.mp3"
      name = "toca"
      isPlaying = false
      imagePath = "../assets/Images/minions.png"
      position = Right
      audioElement = newAudio "../assets/Sounds/tocata.mp3" }

let tac =
    { path = "../assets/Sounds/tac.mp3"
      name = "tac"
      isPlaying = false
      imagePath = "../assets/Images/clav.png"
      position = Left
      audioElement = newAudio "../assets/Sounds/tac.mp3" }

let bull =
    { path = "../assets/Sounds/bull.mp3"
      name = "bull"
      isPlaying = false
      imagePath = "../assets/Images/bulle.png"
      position = Middle
      audioElement = newAudio "../assets/Sounds/bull.mp3" }

let sonnette =
    { path = "../assets/Sounds/sonette.mp3"
      name = "sonnette"
      isPlaying = false
      imagePath = "../assets/Images/sonnette.png"
      position = Right
      audioElement = newAudio "../assets/Sounds/sonette.mp3" }

let tombo =
    { path = "../assets/Sounds/tombo.mp3"
      name = "tombo"
      isPlaying = false
      imagePath = "../assets/Images/tambour.png"
      position = Left
      audioElement = newAudio "../assets/Sounds/tombo.mp3" }

let bol =
    { path = "../assets/Sounds/bol.mp3"
      name = "bol"
      isPlaying = false
      imagePath = "../assets/Images/bol.png"
      position = Middle
      audioElement = newAudio "../assets/Sounds/bol.mp3" }

let marche =
    { path = "../assets/Sounds/marche.mp3"
      name = "marche"
      isPlaying = false
      imagePath = "../assets/Images/marche.png"
      position = Right
      audioElement = newAudio "../assets/Sounds/marche.mp3" }

let av =
    { path = "../assets/Sounds/avengers.mp3"
      name = "av"
      isPlaying = false
      imagePath = "../assets/Images/avengers.png"
      position = Left
      audioElement = newAudio "../assets/Sounds/avengers.mp3" }

let non =
    { path = "../assets/Sounds/gotaga.mp3"
      name = "non"
      isPlaying = false
      imagePath = "../assets/Images/non.png"
      position = Middle
      audioElement = newAudio "../assets/Sounds/gotaga.mp3" }

let coucou =
    { path = "../assets/Sounds/coucou.mp3"
      name = "coucou"
      isPlaying = false
      imagePath = "../assets/Images/coucou.png"
      position = Right
      audioElement = newAudio "../assets/Sounds/coucou.mp3" }

let temp =
    { path = "../assets/Sounds/tempate.mp3"
      name = "temp"
      isPlaying = false
      imagePath = "../assets/Images/temp.png"
      position = Left
      audioElement = newAudio "../assets/Sounds/tempate.mp3" }

let rob =
    { path = "../assets/Sounds/roblox.mp3"
      name = "rob"
      isPlaying = false
      imagePath = "../assets/Images/roblox.png"
      position = Middle
      audioElement = newAudio "../assets/Sounds/roblox.mp3" }

let prout =
    { path = "../assets/Sounds/prout.mp3"
      name = "prout"
      isPlaying = false
      imagePath = "../assets/Images/prout.png"
      position = Right
      audioElement = newAudio "../assets/Sounds/prout.mp3" }

let gifi =
    { path = "../assets/Sounds/gifi.mp3"
      name = "gifi"
      isPlaying = false
      imagePath = "../assets/Images/gifi.png"
      position = Left
      audioElement = newAudio "../assets/Sounds/gifi.mp3" }

let sat =
    { path = "../assets/Sounds/saturer.mp3"
      name = "sat"
      isPlaying = false
      imagePath = "../assets/Images/sat.png"
      position = Middle
      audioElement = newAudio "../assets/Sounds/saturer.mp3" }

let swing =
    { path = "../assets/Sounds/swing.mp3"
      name = "swing"
      isPlaying = false
      imagePath = "../assets/Images/elec.png"
      position = Right
      audioElement = newAudio "../assets/Sounds/swing.mp3" }

let death =
    { path = "../assets/Sounds/death.mp3"
      name = "death"
      isPlaying = false
      imagePath = "../assets/Images/death.png"
      position = Left
      audioElement = newAudio "../assets/Sounds/death.mp3" }

let popo =
    { path = "../assets/Sounds/popo.mp3"
      name = "popo"
      isPlaying = false
      imagePath = "../assets/Images/médoc-liquide.png"
      position = Middle
      audioElement = newAudio "../assets/Sounds/popo.mp3" }

let shot =
    { path = "../assets/Sounds/headshot.mp3"
      name = "shot"
      isPlaying = false
      imagePath = "../assets/Images/headshot.png"
      position = Right
      audioElement = newAudio "../assets/Sounds/headshot.mp3" }

let verre =
    { path = "../assets/Sounds/verre.mp3"
      name = "verre"
      isPlaying = false
      imagePath = "../assets/Images/verre.png"
      position = Left
      audioElement = newAudio "../assets/Sounds/verre.mp3" }

let goute =
    { path = "../assets/Sounds/eau.mp3"
      name = "goute"
      isPlaying = false
      imagePath = "../assets/Images/goute.png"
      position = Middle
      audioElement = newAudio "../assets/Sounds/eau.mp3" }

let coq =
    { path = "../assets/Sounds/coq.mp3"
      name = "coq"
      isPlaying = false
      imagePath = "../assets/Images/coq.png"
      position = Right
      audioElement = newAudio "../assets/Sounds/coq.mp3" }

let poule =
    { path = "../assets/Sounds/poule.mp3"
      name = "poule"
      isPlaying = false
      imagePath = "../assets/Images/poulet.png"
      position = Left
      audioElement = newAudio "../assets/Sounds/poule.mp3" }

let fox =
    { path = "../assets/Sounds/twentyth_century_fox.mp3"
      name = "fox"
      isPlaying = false
      imagePath = "../assets/Images/20th_century_fox.png"
      position = Middle
      audioElement = newAudio "../assets/Sounds/twentyth_century_fox.mp3" }

let engine =
    { path = "../assets/Sounds/engine.mp3"
      name = "engine"
      isPlaying = false
      imagePath = "../assets/Images/ferrari.png"
      position = Right
      audioElement = newAudio "../assets/Sounds/engine.mp3" }

let universal =
    { path = "../assets/Sounds/universal_studios.mp3"
      name = "universal"
      isPlaying = false
      imagePath = "../assets/Images/universal.png"
      position = Left
      audioElement = newAudio "../assets/Sounds/universal_studios.mp3" }

let mario =
    { path = "../assets/Sounds/mario.mp3"
      name = "mario"
      isPlaying = false
      imagePath = "../assets/Images/level.png"
      position = Middle
      audioElement = newAudio "../assets/Sounds/mario.mp3" }

let piece =
    { path = "../assets/Sounds/piece.mp3"
      name = "piece"
      isPlaying = false
      imagePath = "../assets/Images/piece.png"
      position = Right
      audioElement = newAudio "../assets/Sounds/piece.mp3" }

let allSoundPacks =
    [ [ sncf; ah; toca ]
      [ tac; bull; sonnette ]
      [ tombo; bol; marche ]
      [ av; non; coucou ]
      [ temp; rob; prout ]
      [ gifi; sat; swing ]
      [ death; popo; shot ]
      [ verre; goute; coq ]
      [ poule; fox; engine ]
      [ universal; mario; piece ] ]