module App

open Browser.Dom

open Fable
open Fable.Core
open Fable.Core.JsInterop

open Sounds

let buttonsDiv = document.getElementById "buttons"

allSoundPacks |> List.iter (fun soundPack ->
    let line = document.createElement "div" //? Create line of button
    line.classList.add "line"

    soundPack |> List.iter (fun sound ->
        let button = document.createElement "button" //? Create button
        button.classList.add "button"

        let image = document.createElement "img" //? Create image element
        image.setAttribute("src", sound.imagePath)
        image.classList.add "buttonImage"

        button.appendChild image |> ignore //? Put image in button

        line.appendChild button |> ignore //? Put button in line

        button.onclick <- fun _ -> //? Set behavior of button
            if not sound.isPlaying then
                sound.isPlaying <- true
                sound.audioElement.play()

                //? Set "isPlaying" to false when sound is finished
                sound.audioElement.addEventListener("ended", (fun _ -> sound.isPlaying <- false))
            else
                sound.isPlaying <- false

                sound.audioElement.pause()           //? Pause the song
                sound.audioElement.currentTime <- 0. //? and reset time
    )

    buttonsDiv.appendChild line |> ignore //? Put line in all buttons
)


let titleButton = document.getElementById "title"
titleButton.onclick <- fun _ -> //? Set sound of title
    if not Sounds.dejaVu.isPlaying then
        Sounds.dejaVu.isPlaying <- true
        Sounds.dejaVu.audioElement.play()

        //? Set "isPlaying" to false when sound is finished
        Sounds.dejaVu.audioElement.addEventListener("ended", (fun _ -> Sounds.dejaVu.isPlaying <- false))
    else
        Sounds.dejaVu.isPlaying <- false

        Sounds.dejaVu.audioElement.pause()           //? Pause the song
        Sounds.dejaVu.audioElement.currentTime <- 0. //? and reset time


let boomButton = document.getElementById "boomButton"
boomButton.onclick <- fun _ -> //? Set sound of boom button
    if not Sounds.boom.isPlaying then
        Sounds.boom.isPlaying <- true
        Sounds.boom.audioElement.play()

        //? Set "isPlaying" to false when sound is finished
        Sounds.boom.audioElement.addEventListener("ended", (fun _ -> Sounds.boom.isPlaying <- false))
    else
        Sounds.boom.isPlaying <- false

        Sounds.boom.audioElement.pause()           //? Pause the song
        Sounds.boom.audioElement.currentTime <- 0. //? and reset time